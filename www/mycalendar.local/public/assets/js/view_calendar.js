/* CALENDAR */
var  ViewCalendar_CalendarController = {
    init: function () {
        var self = ViewCalendar_CalendarController;
        ViewCalendar_CalendarController._createCalendar();

        $("#CalenderModalNew form").on("submit",function (e) {
            e.preventDefault();
            var form = $(this);
            var url = form.attr('action');

            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(),
                success: function(data)
                {
                    var ntitle = $("#CalenderModalNew input[name='title']").val();
                    var nstart =  $("#CalenderModalNew input[name='dStart']").val();
                    var nend = $("#CalenderModalNew input[name='dEnd']").val();
                    var divCalendar = $('#calendar');
                    divCalendar.fullCalendar('renderEvent', {
                        title:  ntitle,
                        start: nstart,
                        end: nend

                    });

                    $("#CalenderModalNew").modal("hide");
                }
            });
        });

        self._refreshSelectedCals();
        $(".calendar-filter").on("ifUnchecked",function () {
            self._refreshSelectedCals();

        });

        $(".calendar-filter").on("ifChecked",function () {
            self._refreshSelectedCals();

        });

        },
    _listCals: [] ,
    _refreshSelectedCals: function() {
        var self = this;
        self._listCals = [];
        $("input.calendar-filter:checked").each(function (i,elem) {
            self._listCals.push($(elem).attr("data-id"));

        });
        self._refetch_events();
    },
    _refetch_events: function() {
        var self = this;
        self.divCalendar.fullCalendar("refetchEvents");
    },
    _createCalendar: function () {
        var self = ViewCalendar_CalendarController;
        var divCalendar = self.divCalendar = $('#calendar');
        divCalendar.fullCalendar({

            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'agendaWeek,agendaDay'
            },
            // droppable: true,
            // drop: function (date, jsEvent, ui) {
            //
            // },

            defaultDate: null,
            defaultView: "agendaWeek",
            allDaySlot: false,
            contentHeight: 'auto',
            scrollTime: divCalendar.data('start_hour'),
            minTime: divCalendar.data('start_hour'),
            //events: divCalendar.attr('data-events'),
            eventSources: [
                {
                    url: "/calendar/list_events",
                    data: function () {
                        return {
                            cals: self._listCals
                        };
                    }
                }

            ],
            hiddenDays: (function () {

            })(),
            editable: true,
            eventLimit: true,
            slotEventOverlap: false,
            eventRender: function (event, element) {

            },
            selectable: true,
            selectHelper: true,
            select: function(start, end, allDay) {
                $('#fc_create').click();

                $("#CalenderModalNew input[name='title']").val("New Event");
                $("#CalenderModalNew input[name='dStart']").val(start.toISOString());
                $("#CalenderModalNew input[name='dEnd']").val(end.toISOString());


                // $(".antosubmit").on("click", function() {
                //     var title = $("#title").val();
                //     if (end) {
                //         ended = end;
                //     }
                //
                //     categoryClass = $("#event_type").val();
                //
                //     if (title) {
                //         divCalendar.fullCalendar('renderEvent', {
                //                 title: title,
                //                 start: started,
                //                 end: end,
                //                 allDay: allDay
                //             },
                //             true // make the event "stick"
                //         );
                //     }
                //
                //     $('#title').val('');
                //
                //     divCalendar.fullCalendar('unselect');
                //
                //     $('.antoclose').click();
                //
                //     return false;
                // });
            },

        });
    }
};

jQuery(document).ready(function() {
    ViewCalendar_CalendarController.init();
});