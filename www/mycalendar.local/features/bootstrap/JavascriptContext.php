<?php
/**
 * Created by PhpStorm.
 * User: jeremy
 * Date: 12/12/2018
 * Time: 12:50
 */
use Behat\Behat\Context\Context;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Behat\Hook\Scope\AfterScenarioScope;



class JavascriptContext extends MinkContext implements Context
{
    /**
     * Fills in form field with specified id|name|label|value
     * Example: When I fill in "username" with: "bwayne"
     * Example: And I fill in "bwayne" for "username"
     *
     * @override When /^(?:|I )fill in "(?P<field>(?:[^"]|\\")*)" with "(?P<value>(?:[^"]|\\")*)"$/
     * @override When /^(?:|I )fill in "(?P<field>(?:[^"]|\\")*)" with:$/
     * @override When /^(?:|I )fill in "(?P<value>(?:[^"]|\\")*)" for "(?P<field>(?:[^"]|\\")*)"$/
     */
    public function fillField($field, $value)
    {
        $field = $this->fixStepArgument($field);
        $value = $this->fixStepArgument($value);

        $elem = $this->getSession()->getPage()->find("css",$field);
        if(is_null($elem)) {
            throw new Exception("Elem ".$field." not found");
        }
        $elem->setValue($value);

    }

}