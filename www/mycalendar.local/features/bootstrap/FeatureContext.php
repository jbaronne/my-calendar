<?php

use Behat\Behat\Context\Context;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Behat\Hook\Scope\AfterScenarioScope;
/**
 * Defines application features from the specific context.
 */
class FeatureContext extends MinkContext implements Context
{
    private $em;
    private $kernel;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct(\App\Kernel $kernel,\Doctrine\ORM\EntityManager $em)
    {
        $this->kernel = $kernel;
        $this->em = $em;

    }

    /**
     * @Given /^I am authenticated as "([^"]*)" using "([^"]*)"$/
     */
    public function iAmAuthenticatedAs($username, $password) {
        $this->visit('/login');
        $this->fillField('email', $username);
        $this->fillField('password', $password);
        $this->pressButton('Log in');
    }

    /**
     * @When /^wait for the page to be loaded$/
     */
    public function waitForThePageToBeLoaded()
    {
        $this->getSession()->wait(10000, "document.readyState === 'complete'");
    }


    /**
     * Checks, that page contains specified text
     * Example: Then I should see "Who is the Batman?"
     * Example: And I should see "Who is the Batman?"
     *
     * @override Then /^(?:|I )should see "(?P<text>(?:[^"]|\\")*)"$/
     */
    public function assertPageContainsText($text)
    {
//        $actual = $this->getSession()->getPage()->getText();
//        echo 'XPATH PAGE: '. $actual;
        $this->assertSession()->pageTextContains($this->fixStepArgument($text));

    }

    /**
     * Waits a while, for debugging.
     *
     * @param int $seconds
     *   How long to wait.
     *
     * @When I wait :seconds second(s)
     */
    public function wait($seconds) {
        sleep($seconds);
    }


    /**
     * @AfterScenario @register
     */
    public function cleanDB(AfterScenarioScope $scope)
    {
        $john = $this->em->getRepository(\App\Entity\User::class)->findOneBy(array('email'=>'john.doe@gmail.com'));
        $andrew = $this->em->getRepository(\App\Entity\User::class)->findOneBy(array('email'=>'andrew.watson@gmail.com'));

        $users = array();
        $users[] = $john;
        $users[] = $andrew;

        foreach ($users as $createdUser)
        if(!is_null($createdUser)) {
            $this->em->remove($createdUser);
            $this->em->flush();
        }

    }

    /**
     * Fills in form field with specified css selector
     * Example: When I fill in "username" with: "bwayne"
     *
     * @When I fill in :field with :value --css-selector
     *
     */
    public function fillFieldByCss($field, $value)
    {
        $field = $this->fixStepArgument($field);
        $value = $this->fixStepArgument($value);

        $elem = $this->getSession()->getPage()->find("css",$field);
        if(is_null($elem)) {
            throw new Exception("Elem ".$field." not found");
        }
        $elem->setValue($value);

    }


    /**
     * Fills in form field with specified id|name|label|value
     * Example: When I fill in "username" with: "bwayne"
     * Example: And I fill in "bwayne" for "username"
     *
     * @override When /^(?:|I )fill in "(?P<field>(?:[^"]|\\")*)" with "(?P<value>(?:[^"]|\\")*)"$/
     * @override When /^(?:|I )fill in "(?P<field>(?:[^"]|\\")*)" with:$/
     * @override When /^(?:|I )fill in "(?P<value>(?:[^"]|\\")*)" for "(?P<field>(?:[^"]|\\")*)"$/
     */
    public function fillField($field, $value)
    {
//        $actual = $this->getSession()->getPage()->getText();
//        echo 'XPATH page: '. $actual;
        $field = $this->fixStepArgument($field);
        $value = $this->fixStepArgument($value);
        $this->getSession()->getPage()->fillField($field, $value);
    }


    /**
     * @When I press :btn in front of the Calendar :cal
     */
    public function pressCalendarButton($btn,$cal) {

        $cal =  $this->fixStepArgument($cal);
        $btn =  $this->fixStepArgument($btn);

        $td = $this->getSession()->getPage()->find("css","td:contains('$cal')");
        $btn = $td->getParent()->findLink($btn);

        $btn->click();

    }

    /**
     * @AfterScenario @calendar-create
     */
    public function CleanCalendarCreate(AfterScenarioScope $scope)
    {
        $createdCal = $this->em->getRepository(\App\Entity\VCalendar::class)->findOneBy(array('title'=>'Sport'));
        if(!is_null($createdCal)) {
            $this->em->remove($createdCal);
            $this->em->flush();
        }
    }


    /**
     * @AfterScenario @calendar-delete
     */
    public function CleanCalendarDelete(AfterScenarioScope $scope)
    {
        $createdCal = $this->em->getRepository(\App\Entity\VCalendar::class)->findOneBy(array('title'=>'Sport'));
        if(!is_null($createdCal)) {
            $this->em->remove($createdCal);
            $this->em->flush();
        }
    }


    /**
     * @AfterScenario @calendar-edit
     */
    public function CleanCalendarEdit(AfterScenarioScope $scope)
    {
        $createdCal = $this->em->getRepository(\App\Entity\VCalendar::class)->findOneBy(array('title'=>'Baseball - Edited'));
        if(!is_null($createdCal)) {
            $this->em->remove($createdCal);
            $this->em->flush();
        }
    }

    /**
     * @AfterScenario @event-create
     */
    public function CleanEventCreate(AfterScenarioScope $scope)
    {
        $createdCal = $this->em->getRepository(\App\Entity\VCalendar::class)->findOneBy(array('title'=>'Social Events'));
        if(!is_null($createdCal)) {
            $this->em->remove($createdCal);
            $this->em->flush();
        }
    }
    /**
     * @Then The field :field should contain :value
     */
    public function FieldShouldContain($field,$value) {
        $field = $this->fixStepArgument($field);
        $value = $this->fixStepArgument($value);

        $elem = $this->getSession()->getPage()->findField($field);
        if(is_null($elem))
            throw new Exception("Element ".$field."not found");
        if($elem->getValue() !== $value)
            throw new Exception("Field ".$field." should contain ".$value.", instead it contains ".$elem->getValue());

    }

    /**
     * @Then I click on :el --css-selector
     */
    public function clickCssSelector($el)
    {
        $el = $this->fixStepArgument($el);
        $elem = $this->getSession()->getPage()->find("css",$el);

        if(is_null($elem)) {
            throw new Exception("Elem ".$elem." not found");
        }
        $elem->click();
    }
    /**
     * @Then I fill date field :field with today date at hour :value
     */
    public function fillDateWithHour($field,$value)
    {
        $field = $this->fixStepArgument($field);
        $value = $this->fixStepArgument($value);
        $value = date("Y-m-d") ."T".$value;
        $this->fillField($field,$value);
    }


}
