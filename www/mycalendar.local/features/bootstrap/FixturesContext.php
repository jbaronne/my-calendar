<?php

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;
use \Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Testwork\Hook\Scope\BeforeSuiteScope;
use Behat\Behat\Hook\Scope\AfterScenarioScope;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use \Behat\MinkExtension\Context\RawMinkContext;

class FixturesContext extends RawMinkContext implements Context
{
    public $em;
    public $kernel;
    public $userService;
    public $env;

    public function __construct(\App\Kernel $kernel,\App\Services\UserService $userService,\Doctrine\ORM\EntityManager $em,$env)
    {
        $this->kernel = $kernel;
        $this->userService = $userService;
        $this->em = $em;
        $this->env = $env;
    }

    /**
     * @Given there are users:
     */
    public function thereAreUsers(TableNode $userNodes)
    {
        foreach ($userNodes as $u) {
            $user = new \App\Entity\User();
            $user->setUsername($u['username']);
            $user->setFullName($u['username']);
            $user->setPassword($this->userService->encodeUserPassword($user,$u['password']));
            $user->setEmail($u['email']);

            $this->em->persist($user);
            // $row['username'], $row['password'], $row['email']
        }
        $this->em->flush();
    }

    /**
     * @Given there are calendars:
     */
    public function thereAreCalendars(TableNode $calsNode)
    {
        $owner = $this->em->getRepository(\App\Entity\User::class)->findOneBy(array('email'=>'jimi.woods@gmail.com'));

        foreach ($calsNode as $c) {
            $cal = new \App\Entity\VCalendar();
            $cal->setTitle($c['title']);
            $cal->setDescription($c['description']);
            $cal->setDefaultColor($c['color']);
            $cal->setOwner($owner);
            $this->em->persist($cal);

        }

        $this->em->flush();
    }


    /**
     * @BeforeSuite
     */
    public static function prepare(BeforeSuiteScope $scope)
    {
        if($scope->getSuite()->getName() !== "fixtures")
            return;

        $settings = $scope->getEnvironment()->getSuite()->getSettings();
        $env = $settings['contexts'][0]['FixturesContext']['env'];

        $kernel = new \App\Kernel($env, true);
        $kernel->boot();
        echo "Kernel env : ".$kernel->getEnvironment();

        $application = new Application($kernel);
        echo $application->getLongVersion();
        $application->setAutoExit(false);

        FixturesContext::runConsole($application,'doctrine:database:create',['--if-not-exists' => true]);
        FixturesContext::runConsole($application, 'doctrine:schema:drop', ['--force' => true, '--full-database' => true]);
        FixturesContext::runConsole($application, 'doctrine:schema:create');

        $kernel->shutdown();

    }


    private static function runConsole(\Symfony\Component\Console\Application $application, $command, $options = [],$out=null)
    {
        //$options['-e'] = 'test';
        $options['-q'] = null;
        $options = array_merge($options, ['command' => $command]);

        if($out == null) {
            $out = new \Symfony\Component\Console\Output\BufferedOutput();
        }

        print_r($command);
        $code = $application->doRun(new \Symfony\Component\Console\Input\ArrayInput($options),$out);
        print $out->fetch();

        return $code;
    }
}