@login
Feature: Login in
  In order to access my App
  As an User
  I need to enter a login and a password


  Scenario: Login In With good credentials
    Given I am on "/login"
    When I fill in "email" with "jimi.woods@gmail.com"
    And I fill in "password" with "kitten"
    And I press "Log in"
    Then I should see "Welcome, Jimi Woods"

  Scenario: Login In With bad mail
    Given I am on "/login"
    When I fill in "email" with "idontexists.@gmail.com"
    And I fill in "password" with "rien"
    And I press "Log in"
    Then I should see "Email could not be found"

  Scenario: Login In With bad password
    Given I am on "/login"
    When I fill in "email" with "jimi.woods@gmail.com"
    And I fill in "password" with "bad password"
    And I press "Log in"
    Then I should see "Invalid credentials"



