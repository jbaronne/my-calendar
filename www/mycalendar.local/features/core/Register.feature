@register 
Feature: Registering
  In order to access my App
  As an User
  I need to sign in the app

  Scenario: Registering a new User with available email
    Given I am on "/login"
    When I follow "Create Account"
    When I fill in "email-register" with "john.doe@gmail.com"
    And I fill in "username" with "John DOE"
    And I fill in "password-register" with "verysecret"
    And I press "Submit"
    And I should see "Welcome, John DOE"