@calendar
Feature: Manage Calendar
    In Order to Create Events
    As an User
    I Should be able to manage my calendars first
    Create, Edit and Delete

 @calendar-create
  Scenario: Create a Calendar with valid inputs
    Given I am authenticated as "jimi.woods@gmail.com" using "kitten"
    Given I am on "/calendar/create"
    When I fill in "form_title" with "Sports"
    And  I fill in "form_description" with "This is the calendar i use to program my sport events"
    And I fill in "form_defaultColor" with "#FF00FF"
    Then I press "Create Calendar"
    When I am on "/calendar/list"
    Then I should see "Sports"

  @calendar-delete
  Scenario: Delete an existing Calendar
    Given there are calendars:
      |title  | description       | color   |
      | Running | My footing calendar   | #EEAA11 |
    Given I am authenticated as "jimi.woods@gmail.com" using "kitten"
    Given I am on "/calendar/list"
    When I press "Delete" in front of the calendar "Running"
    Then I should not see "Running"

  @calendar-edit
  Scenario: Delete an existing Calendar
    Given there are calendars:
      |title  | description       | color   |
      | Baseball | My baseball events   | #EEAA11 |
    Given I am authenticated as "jimi.woods@gmail.com" using "kitten"
    Given I am on "/calendar/list"
    When I press "Edit" in front of the calendar "Baseball"
    And I fill in "form_title" with "Baseball - Edited"
    And I fill in "form_description" with "My baseball events - Edited"
    And I fill in "form_defaultColor" with "#00FF00"
    Then I press "Save"
    When  I am on "/calendar/list"
    When I press "Edit" in front of the calendar "Baseball - Edited"
    Then The field "form_title" should contain "Baseball - Edited"
    And The field "form_description" should contain "My baseball events - Edited"
    And The field "form_defaultColor" should contain "#00FF00"

