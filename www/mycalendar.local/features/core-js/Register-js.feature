@register @javascript
Feature: Registering
  In order to access my App
  As an User
  I need to sign in the app

  Scenario: Registering a new User with available email
    Given I am on "/login"
    When I follow "Create Account"
    When I fill in using css "#register input[name='email']" with "andrew.watson@gmail.com"
    And I fill in using css "#register input[name='username']" with "Andrew WATSON"
    And I fill in using css "#register input[name='password']" with "verysecret"
    And I press "Submit"
    And I should see "Welcome, Andrew WATSON"