@event @javascript
Feature: Create Event
  Create event on calendar

  @event-create
  Scenario: Create footbal event today at 6pm
    Given there are calendars:
      |title  | description       | color   |
      | Social Events | Me hanging out with friends   | #000000 |
    Given I am authenticated as "jimi.woods@gmail.com" using "kitten"
    Given I am on the homepage
    Then wait for the page to be loaded
    When I click on "td.fc-widget-content" --css-selector
    And I wait 1 second
    And I fill date field "dStart" with today date at hour "17:00:00"
    And I fill date field "dEnd" with today date at hour "18:00:00"
    And I fill in "title" with "Footbal with friends"
    And I select "Social Events" from "calendar"
    When I press "Save changes"
    And I wait 1 second
    Then I should see "Footbal with friend"

