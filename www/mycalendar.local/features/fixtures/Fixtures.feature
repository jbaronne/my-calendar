Feature: Fixtures
  In order to test my App
  As an Developer
  I need to generate data


  Scenario: Create Users
    Given there are users:
    | username        | password | email                    |
    | Jimi Woods      | kitten   | jimi.woods@gmail.com     |
    | God Himself     | gabriel  | god@gmail.com            |

  Scenario: Create Calendars
    Given there are calendars:
    | title  | description      | color   |
    | School | My Courses       | #FF0000 |
    | Extra  | My Extra Courses | #00FF00 |