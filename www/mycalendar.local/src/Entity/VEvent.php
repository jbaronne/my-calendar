<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\VEventRepository")
 * @ORM\Table(name="vEvent")
 */
class VEvent  
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $dtTitle;

    /** @ORM\Column(type="datetime") */
    private $dtStart;
    /** @ORM\Column(type="datetime") */
    private $dtEnd;

    /** @ORM\Column(type="string") */
    private $tzID;



    /**
     * @ORM\ManyToOne(targetEntity="VCalendar", inversedBy="events")
     * @ORM\JoinColumn(name="calendar_id", referencedColumnName="id", onDelete="CASCADE")
     *
     */

    private $calendar;

    /**
     * @return mixed
     */
    public function getCalendar()
    {
        return $this->calendar;
    }

    /**
     * @param mixed $calendar
     */
    public function setCalendar($calendar): void
    {
        $this->calendar = $calendar;
    }



    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }


    public function setTzID($tzID): void
    {
        $this->tzID = $tzID;
    }


    /**
     * @return mixed
     */
    public function getDtTitle()
    {
        return $this->dtTitle;
    }

    /**
     * @param mixed $dtTitle
     */
    public function setDtTitle($dtTitle): void
    {
        $this->dtTitle = $dtTitle;
    }

    /**
     * @return mixed
     */
    public function getDtStart()
    {
        return $this->dtStart;
    }

    /**
     * @param mixed $dtStart
     */
    public function setDtStart($dtStart): void
    {
        $this->dtStart = $dtStart;
    }

    /**
     * @return mixed
     */
    public function getDtEnd()
    {
        return $this->dtEnd;
    }

    /**
     * @param mixed $dtEnd
     */
    public function setDtEnd($dtEnd): void
    {
        $this->dtEnd = $dtEnd;
    }

    /**
     * @return mixed
     */
    public function getTzID()
    {
        return $this->tzID;
    }

    /**
     * @param mixed $tzID
     */
}