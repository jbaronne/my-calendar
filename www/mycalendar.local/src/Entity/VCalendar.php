<?php
/**
 * Created by PhpStorm.
 * User: jeremy
 * Date: 28/11/2018
 * Time: 10:41
 */

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;



/**
 * @ORM\Entity(repositoryClass="App\Repository\VCalendarRepository")
 * @ORM\Table(name="vCalendar")
 */
class VCalendar
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\OneToMany(targetEntity="VEvent", mappedBy="calendar")
     */
    private $events;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="calendars")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     **/
    private $owner;


    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    private $title;
    /**
     * @return int
     */


    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $description;


    /**
     * @var string
     * @ORM\Column(type="string",name="default_color")
     *
     */
    private $defaultColor;

    /**
     * @return string
     */
    public function getDefaultColor(): string
    {
        return $this->defaultColor;
    }

    /**
     * @param string $defaultColor
     */
    public function setDefaultColor(string $defaultColor): void
    {
        $this->defaultColor = $defaultColor;
    }


    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }




    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }


    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * @param mixed $events
     */
    public function setEvents($events): void
    {
        $this->events = $events;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner): void
    {
        $this->owner = $owner;
    }



}