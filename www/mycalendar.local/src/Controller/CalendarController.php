<?php
/**
 * Created by PhpStorm.
 * User: jeremy
 * Date: 28/11/2018
 * Time: 11:45
 */

namespace App\Controller;

use App\Entity\VCalendar;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
class CalendarController extends AbstractController
{
    /**
     * @Route("/calendar/list", name="calendar_list")
     */
    public function list(): Response
    {
        $user = $this->getUser();
        $calendars = $user->getCalendars();


        return $this->render('calendar/list.html.twig', [
        'calendars' =>  $calendars,
        'user' => $user
    ]);
    }

    /**
     * @Route("/calendar/create", name="calendar_create" )
     */
    public function viewCreate(Request $request): Response
    {
        $cal = new VCalendar();
        $cal->setTitle('New Calendar');
        $cal->setDefaultColor("#EEEE");
        $form = $this->createFormBuilder($cal)
            ->add('title', TextType::class)
            ->add('description', TextareaType::class,array('required' => false,  'empty_data' => ''))
            ->add('defaultColor',TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Create Calendar',))
            ->getForm();

        if($request->isMethod('POST')) {
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()  ) {
                $em = $this->getDoctrine()->getManager();

                $cal->setOwner($this->getUser());

                $em->persist($cal);
                $em->flush();
                return $this->redirectToRoute('calendar_list');
            }
        }

        return $this->render('calendar/create.html.twig', array(
            'form' => $form->createView(),
            'user'=> $this->getUser()
        ));

    }

    /**
     * @Route("/calendar/edit/{id_calendar}", name="calendar_edit" )
     */
    public function viewEdit( Request $request,$id_calendar): Response
    {
        $em = $this->getDoctrine()->getManager();
        $cal = $em->getRepository(VCalendar::class)->find($id_calendar);

        $form = $this->createFormBuilder($cal)
            ->add('title', TextType::class)
            ->add('description', TextareaType::class,array('required' => false,  'empty_data' => ''))
            ->add('defaultColor',TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Save',))
            ->getForm();


        if($request->isMethod('POST')) {
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()  ) {
                $em->persist($cal);
                $em->flush();
                return $this->redirectToRoute('calendar_list');
            }
        }


        return $this->render('calendar/create.html.twig', array(
            'form' => $form->createView(),
            'user'=> $this->getUser()
        ));

    }



    /**
     * @Route("/calendar/delete/{id_calendar}", name="calendar_delete" )
     */
    public function deleteAction( Request $request,$id_calendar): Response
    {
        $em = $this->getDoctrine()->getManager();
        $cal = $em->getRepository(VCalendar::class)->find($id_calendar);

        $em->remove($cal);
        $em->flush();


        if($request->isXmlHttpRequest()) {
            return new JsonResponse(array('ok'=>true,'deleted'=>$id_calendar));
        }
        else {
            return $this->redirectToRoute('calendar_list');
        }



    }


}