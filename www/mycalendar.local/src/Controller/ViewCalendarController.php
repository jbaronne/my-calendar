<?php

namespace App\Controller;

use App\Entity\VCalendar;
use App\Entity\VEvent;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ViewCalendarController extends AbstractController
{
    /**
     * @Route("/view/calendar", name="view_calendar")
     */
    public function index()
    {
        $user = $this->getUser();
        $cals = $user->getCalendars();

        return $this->render('view_calendar/index.html.twig', [
            'controller_name' => 'ViewCalendarController',
            'calendars'=> $cals,
            'user' => $user
        ]);
    }
    /**
     * @Route("/calendar/create_event", name="create_event")
     */
    public function createEvent( Request $request) {
        $user = $this->getUser();


        $title = $request->request->get("title");
        $dStart = $request->request->get("dStart");
        $dEnd = $request->request->get("dEnd");
        $calID = $request->request->get("calendar");

        $event = new VEvent();
        $event->setDtTitle($title);
        $event->setDtStart(new \DateTime($dStart) );
        $event->setDtEnd(new \DateTime($dEnd) );
        $event->setTzID("Europe/Paris");

        $calendar = $this->getDoctrine()->getRepository(VCalendar::class)->find($calID);
        if(isset($calendar) && $calendar->getOwner()->getId() == $user->getId()) {
            $event->setCalendar($calendar);
        }
        else
            return new Response("nok : ");

        $em = $this->getDoctrine()->getManager();
        $em->persist($event);
        $em->flush();

        return new Response("ok : ".$event->getId());
    }
    /**
     * @Route("/calendar/list_events", name="list_events")
     */
    public function listJsonEvents (Request $request) {
        $user = $this->getUser();
        $start =   \DateTime::createFromFormat('Y-m-d',  $request->query->get("start"));
        $end =   \DateTime::createFromFormat('Y-m-d',  $request->query->get("end"));

        $cals =   $request->query->get("cals");

        $out = array();

        if(!empty($cals)) {

            $cals = array_map('intval',$cals);
            $events = $this->getDoctrine()->getRepository(VEvent::class)->findEventsForPeriod($user,$start,$end,$cals);

            $func = function(VEvent $event) {
                return [
                    'id' => $event->getId()
                    ,'title'=> $event->getDtTitle()
                    ,'start'=>$event->getDtStart()->format(\DateTime::ISO8601)
                    ,'end'=>$event->getDtEnd()->format(\DateTime::ISO8601)
                    ,'color'=>$event->getCalendar()->getDefaultColor()
                ];
            };

            $out = array_map($func,$events);
        }

        return new JsonResponse($out);


    }


}
