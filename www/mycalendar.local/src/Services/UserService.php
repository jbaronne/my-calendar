<?php
/**
 * Created by PhpStorm.
 * User: jeremy
 * Date: 06/12/2018
 * Time: 10:32
 */

namespace App\Services;


use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserService
{

    private $encoder;
    /**
     * UserService constructor.
     */
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }


    public function encodeUserPassword(User $user,string $password) : string
    {
        return $this->encoder->encodePassword($user, $password);

    }


}