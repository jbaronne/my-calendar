<?php
/**
 * Created by PhpStorm.
 * User: jeremy
 * Date: 29/11/2018
 * Time: 12:28
 */

namespace App\Repository;


use App\Entity\VCalendar;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class VCalendarRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, VCalendar::class);
    }

}