<?php
/**
 * Created by PhpStorm.
 * User: jeremy
 * Date: 27/11/2018
 * Time: 10:28
 */

namespace App\Repository;


use App\Entity\VEvent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Validator\Constraints\Date;


class VEventRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, VEvent::class);
    }

    /**
     * @return VEvent[]
     */
    public function findEventsForPeriod(\App\Entity\User $user,\DateTime $start, \DateTime $end,  $cals)
    {
        $expr = $this->getEntityManager()->getExpressionBuilder();

        $queryBuilder = $this->createQueryBuilder('e')
           /// ->select('e')
            ->where('e.dtStart >= :start AND e.dtEnd <= :end')
            ->setParameter('start',$start)
            ->setParameter('end',$end)
           // ->setParameters(['start'=>$start,'end'=>$end])
            ->leftJoin('e.calendar','c')
            ->leftJoin('c.owner','u')
            ->andWhere('u.id = :uid')
            ->setParameter('uid',$user->getId())
            ->andWhere('c.id IN (:calIds)')
            ->setParameter('calIds',$cals,\Doctrine\DBAL\Connection::PARAM_INT_ARRAY);



        return $queryBuilder->getQuery()->getResult();


    }
}



